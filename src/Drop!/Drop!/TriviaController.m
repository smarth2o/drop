//
//  TriviaController.m
//  Drop!
//
//  Created by Fausto Dassenno on 13/10/14.
//  Copyright (c) 2014 MSM. All rights reserved.
//

#import "TriviaController.h"
#import "QuestionsRepo.h"

@interface TriviaController ()

@end

@implementation TriviaController

-(id)initWithFrame:(CGRect)frame {
    
    if(self == [super init]) {
        self.view.frame=frame;
    }
    return self;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    UIImage *bg=[UIImage imageNamed:@"bg_clear.png"];
    UIImageView* bg_i = [[UIImageView alloc] initWithImage:bg];
    bg_i.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:bg_i];
    
    question = [QuestionsRepo getQuestion];
    
    UILabel* info=[[UILabel alloc] initWithFrame:CGRectMake(40, 120, 230, 200)];
    info.text=[question valueForKey:@"question"];
    info.textAlignment = NSTextAlignmentCenter;
    info.numberOfLines=10;
    info.textColor=[UIColor colorWithRed:0 green:114.0/255.0 blue:130.0/255.0 alpha:1];
    [self.view addSubview:info];
    
    NSArray* answers = [question valueForKey:@"answers"];
    float butt_start = 250;
    int inter=0;
    for(NSString* a in answers) {
        UIButton* answer_button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [answer_button setTag:inter];
        [answer_button setBackgroundImage:[UIImage imageNamed:@"button_bg.png"] forState:UIControlStateNormal];
        [answer_button setBackgroundImage:[UIImage imageNamed:@"button_bg_clear.png"] forState:UIControlStateSelected];
        [answer_button setTitle:a forState:UIControlStateNormal];
        [[answer_button titleLabel] setLineBreakMode:NSLineBreakByWordWrapping];
        [[answer_button titleLabel] setTextAlignment:NSTextAlignmentCenter];
        answer_button.frame = CGRectMake(19, butt_start+inter*56, 282, 51);
        [answer_button addTarget:self action:@selector(check:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:answer_button];
        inter++;
    }
    
}

-(void) check:(id)sender {
    if([[question valueForKey:@"correct"] integerValue]==[sender tag]) {
        
        UIImage* correct_i =[UIImage imageNamed:@"correct.png"];
        UIImageView* correct_iv = [[UIImageView alloc] initWithImage:correct_i];
        correct_iv.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [self.view addSubview:correct_iv];
        
        UILabel* info=[[UILabel alloc] initWithFrame:CGRectMake(40, 250, 230, 200)];
        info.text=@"Correct!!\nAdd 10 points to your score";
        info.textAlignment = NSTextAlignmentCenter;
        info.font=[UIFont fontWithName:@"Verdana" size:24];
        info.numberOfLines=4;
        info.textColor=[UIColor colorWithRed:0 green:114.0/255.0 blue:130.0/255.0 alpha:1];
        [self.view addSubview:info];
        
        UIButton* cancel_b = [UIButton buttonWithType:UIButtonTypeCustom];
        [cancel_b setImage:[UIImage imageNamed:@"button_bg_back.png"] forState:UIControlStateNormal];
        cancel_b.frame = CGRectMake(19, self.view.frame.size.height-80, 282, 51);
        [cancel_b addTarget:self action:@selector(cancel_do) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:cancel_b];
        
    } else {
        
        UIImage* correct_i =[UIImage imageNamed:@"wrong.png"];
        UIImageView* correct_iv = [[UIImageView alloc] initWithImage:correct_i];
        correct_iv.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [self.view addSubview:correct_iv];
        
        UILabel* info=[[UILabel alloc] initWithFrame:CGRectMake(40, 20, 230, 200)];
        info.text=@"Wrong!!\nSorry, no cookies for you this time.";
        info.textAlignment = NSTextAlignmentCenter;
        info.font=[UIFont fontWithName:@"Verdana" size:24];
        info.numberOfLines=4;
        info.textColor=[UIColor colorWithRed:0 green:114.0/255.0 blue:130.0/255.0 alpha:1];
        [self.view addSubview:info];
        
        UIButton* cancel_b = [UIButton buttonWithType:UIButtonTypeCustom];
        [cancel_b setImage:[UIImage imageNamed:@"button_bg_back.png"] forState:UIControlStateNormal];
        cancel_b.frame = CGRectMake(19, self.view.frame.size.height-80, 282, 51);
        [cancel_b addTarget:self action:@selector(cancel_do) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:cancel_b];
        
    }
}

-(void)cancel_do {
    [self dismissViewControllerAnimated:self completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
