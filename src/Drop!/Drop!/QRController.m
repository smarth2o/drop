//
//  QRController.m
//  Drop!
//
//  Created by Fausto Dassenno on 12/10/14.
//  Copyright (c) 2014 MSM. All rights reserved.
//

#import "QRController.h"
#import "TriviaController.h"

@interface QRController ()

@end

@implementation QRController

-(id)initWithFrame:(CGRect)frame {
    
    if(self == [super init]) {
        self.view.frame=frame;
    }
    return self;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor=[UIColor whiteColor];
    UIImage *bg=[UIImage imageNamed:@"bg_clear.png"];
    UIImageView* bg_i = [[UIImageView alloc] initWithImage:bg];
    bg_i.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:bg_i];
    
    
    UILabel* info=[[UILabel alloc] initWithFrame:CGRectMake(50, 200, 220, 70)];
    info.text=@"Point the wasteful card, try to convert it in saving one and hear points!";
    info.numberOfLines=3;
    info.textColor=[UIColor colorWithRed:0 green:114.0/255.0 blue:130.0/255.0 alpha:1];
    [self.view addSubview:info];
    
    UIButton* single = [UIButton buttonWithType:UIButtonTypeCustom];
    [single setImage:[UIImage imageNamed:@"button_scan.png"] forState:UIControlStateNormal];
    single.frame = CGRectMake(19, self.view.frame.size.height-140, 282, 51);
    [single addTarget:self action:@selector(scan_do) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:single];
    
    UIButton* cancel_b = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancel_b setImage:[UIImage imageNamed:@"button_cancel.png"] forState:UIControlStateNormal];
    cancel_b.frame = CGRectMake(19, self.view.frame.size.height-80, 282, 51);
    [cancel_b addTarget:self action:@selector(cancel_do) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cancel_b];
    
}

-(void)cancel_do {
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)scan_do {
    self.view.backgroundColor=[UIColor whiteColor];
    ZBarReaderViewController *codeReader = [ZBarReaderViewController new];
    codeReader.readerDelegate=self;
    codeReader.supportedOrientationsMask=ZBarOrientationMaskAll;
    
    ZBarImageScanner *scanner= codeReader.scanner;
    [scanner setSymbology:ZBAR_I25 config:ZBAR_CFG_ENABLE to:0];
    
    [self presentViewController:codeReader animated:YES completion:nil];
}


-(void)viewDidAppear:(BOOL)animated {

    
}


- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        break;
    
    NSLog(@"%@",symbol.data);
    NSString* code = symbol.data;
    
    // dismiss the controller
    [reader dismissViewControllerAnimated:YES completion:^ {
        TriviaController* trivia = [[TriviaController alloc] initWithFrame:self.view.frame];
        [self presentViewController:trivia animated:NO completion:nil];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
