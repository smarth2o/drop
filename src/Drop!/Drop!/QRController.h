//
//  QRController.h
//  Drop!
//
//  Created by Fausto Dassenno on 12/10/14.
//  Copyright (c) 2014 MSM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBarSDK.h"

@interface QRController : UIViewController <ZBarReaderDelegate>

-(id)initWithFrame:(CGRect)frame;

@end
