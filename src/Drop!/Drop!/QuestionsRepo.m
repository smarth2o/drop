//
//  QuestionsRepo.m
//  Drop!
//
//  Created by Fausto Dassenno on 13/10/14.
//  Copyright (c) 2014 MSM. All rights reserved.
//

#import "QuestionsRepo.h"

@implementation QuestionsRepo

+(NSDictionary*)getQuestion {
    
    NSArray* questions = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"questions" ofType:@"plist"]];
    
    NSUInteger randomIndex = arc4random() % [questions count];
    
    return [questions objectAtIndex:randomIndex];
}

@end
