﻿using UnityEngine;
using System.Collections;
using System;
using System.Text;
using System.Collections.Generic;
using SmartLocalization;
using System.Net.NetworkInformation;
using UnityEngine.UI;
using MiniJSON;
using System.Text.RegularExpressions;

public class GameController : MonoBehaviour {

	public static bool isUnity = false;

	public static bool inetAvail;

	public static bool isAuthenticated;
	public static bool isSkipped;

	public Text connection;
	public GameObject wifiStatus;
	public Text invalidUsername;
	public InputField username;
	public InputField password;

	public InputField registrationUsername;
	public InputField registrationPassword;
	public InputField doublePassword;
	public InputField nickname;
	public Text registrationError;

	public CanvasGroup prev;
	public CanvasGroup next;
	public CanvasGroup net;
	public CanvasGroup message;
    public Text messageText;

	public static CanvasGroup previous;
	public static CanvasGroup nextStd;
	public static CanvasGroup nextNet = null;

	public static bool debug = false;

	public GameObject registrationButton;

	public static int totalPoints = 0;

	private static DropCodeScanner scanner;

	public static string chosenEmail=null;
	public static string chosenPassword=null;

	private static string currentSessionId = null;

	public static string serverAddress = "sh2oge.lucagalli.me";

	public static String basePath = "https://"+serverAddress+"/api";

	private static string geBasePath = "http://131.175.141.236:8080/";

	private static string geActionPath = "community/UserActivityCreditWebServiceREST/AssignActionsToUsersByMail/assignActionsToUsers";

	private static GameController mInstance = null;

	private bool mInitialized = false;

	private static string adminUser = "admin";

	private static string adminPassword = "adminsh2o";


	private static GameController instance
	{
		get
		{
			if (mInstance == null)
			{
				mInstance = GameObject.FindObjectOfType(typeof(GameController)) as GameController;
				
				if (mInstance == null)
				{
					mInstance = new GameObject("GameController").AddComponent<GameController>();
				}
			}
			return mInstance;
		}
	}
	

	void Awake()
	{
		string cert1 = "-----BEGIN CERTIFICATE-----\n" +
						"MIICjTCCAfYCCQDgQI5woTaTHDANBgkqhkiG9w0BAQsFADCBijELMAkGA1UEBhMC\n" +
						"SVQxDTALBgNVBAgMBENvbW8xDTALBgNVBAcMBENvbW8xFjAUBgNVBAoMDU1vb25z\n" +
						"dWJtYXJpbmUxHDAaBgNVBAMME3NoMm9nZS5sdWNhZ2FsbGkubWUxJzAlBgkqhkiG\n" +
						"9w0BCQEWGGxnYWxsaUBtb29uc3VibWFyaW5lLmNvbTAeFw0xNTA3MjMxNTMzMDRa\n" +
						"Fw0xNjA3MjIxNTMzMDRaMIGKMQswCQYDVQQGEwJJVDENMAsGA1UECAwEQ29tbzEN\n" +
						"MAsGA1UEBwwEQ29tbzEWMBQGA1UECgwNTW9vbnN1Ym1hcmluZTEcMBoGA1UEAwwT\n" +
						"c2gyb2dlLmx1Y2FnYWxsaS5tZTEnMCUGCSqGSIb3DQEJARYYbGdhbGxpQG1vb25z\n" +
						"dWJtYXJpbmUuY29tMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCjDvRid0eJ\n" +
						"j1TBNZGxuPhZ5S23lDv1uhVacciwmyTtsINARYn1WQO9VoZ0CuhVpY3+oSw0+eFl\n" +
						"0qkBBbUesLoUkT9NgvgQhb5TBhua5QB9JT1uwqITdKwkVMOoR8A9PfqrQhXwiC0f\n" +
						"i9cQbnCQeInSV7QWXuVyE4jDt/omCAtFfQIDAQABMA0GCSqGSIb3DQEBCwUAA4GB\n" +
						"AAQOkZ3pHcEIsv/CaK6Xe1/xAos7FnKTWhOMrm7D58DK3LCf8o5M8QB18Z45SFJe\n" +
						"QVLdScVFOElNeQ9shrYzYU6q+LNBHR8Zn4c/9cBcYcD4mketUxDDruoT98QpmWSl\n" +
						"WUoGtdu6L7WgDsJNmECi2eKO9fqeT5dIaWL5tf3nztX5\n" +
						"-----END CERTIFICATE-----";

		
		AndroidHttpsHelper.AddCertificate(cert1);
		mInitialized = true;

		if (mInstance == null)
		{
			mInstance = this as GameController;
		}
	}


	// Use this for initialization
	void Start () {
		Debug.Log (Application.systemLanguage.ToString ());
		LanguageManager languageManager = LanguageManager.Instance;
		languageManager.defaultLanguage = "en";
		if (Application.systemLanguage.ToString () == "Italian")
						languageManager.ChangeLanguage ("it");

		if(mInitialized == false)
		{
			Debug.LogError("Initialization failed. Default WWW class is used.");
		}
		previous = prev;
		nextStd = next;
		nextNet = net;
		isAuthenticated = false;
		isSkipped = false;
		scanner = GetComponent<DropCodeScanner>();
	}


	public void loginUser() {
		if(!(password.Equals("")||username.Equals("")))
			instance.StartCoroutine(loginUserRoutine(username.text, password.text));
	}


	public void registerUser() {
		if (!(registrationPassword.Equals ("") || registrationUsername.Equals ("") || (!registrationPassword.text.Equals (doublePassword.text)) || nickname.Equals ("")))
					instance.StartCoroutine (registrationUserRoutine (registrationUsername.text, registrationPassword.text, nickname.text));
				else if (registrationPassword.Equals ("")) {
						if (!LanguageManager.Instance.LoadedLanguage.Equals ("it"))
							registrationError.text = "Password required!";
						else
							registrationError.text = "Password richiesta!";
				} else if (registrationUsername.Equals ("")) {
						if (!LanguageManager.Instance.LoadedLanguage.Equals ("it"))
							registrationError.text = "Password required!";
						else
							registrationError.text = "Password richiesta!";
				} else if (!registrationPassword.text.Equals (doublePassword.text)) {
					if (!LanguageManager.Instance.LoadedLanguage.Equals ("it"))
						registrationError.text = "The passwords do not match!";
					else
						registrationError.text = "Le password sono diverse!";
				} else if (nickname.Equals ("")) {
					if (!LanguageManager.Instance.LoadedLanguage.Equals ("it"))
						registrationError.text = "Nickname required!";
					else
						registrationError.text = "Nickname richiesto!";
				}
	}

	public void quitGame() {
		if (isAuthenticated) {
						instance.StartCoroutine (cgpsRoutine (currentSessionId));
						currentSessionId = null;
				}
	}


	IEnumerator registrationUserRoutine(String username, String password, String nickname) {
		WWWForm form = new WWWForm();
		Dictionary<string,string> headers = form.headers;
		username = username.Trim ();
		username = username.ToLower ();
		password = password.Trim ();
		nickname = nickname.Trim ();
		form.AddField ("email", username);
		form.AddField ("password", password);
		form.AddField ("nickname", nickname);
		form.AddField ("role", "player");
		byte[] rawData = form.data;
		
		
		
		
		WWW www = new WWW(basePath+"/players/", rawData, headers);
		
		float elapsedTime = 0.0f;
		
		while (!www.isDone) {
			elapsedTime += Time.deltaTime;
			
			if (elapsedTime >= 10.0f) break;
			
			yield return null;  
		}

		
		if (!www.isDone || !string.IsNullOrEmpty (www.error)) {
			Debug.LogError (string.Format ("Failed Register call!\n{0}", www.error));
			if(!LanguageManager.Instance.LoadedLanguage.Equals("it"))
				registrationError.text = "Error in the registration, try later!";
			else
				registrationError.text = "Errore di registrazione, riprova più tardi!";
			yield break;
		} else {
			GlobalUtils utils = registrationButton.GetComponent<GlobalUtils>();
			utils.nextMenu();
		}
		
	}


	IEnumerator loginUserRoutine(String username, String password) {
		Dictionary<string,string> headers = new Dictionary<string,string>();
		headers.Add("Content-Type", "application/json");
		username = username.Trim ();
		username = username.ToLower ();
		password = password.Trim ();
		headers["Authorization"] = "Basic " + System.Convert.ToBase64String(
			System.Text.Encoding.ASCII.GetBytes(username+":"+password));
		
		
		WWW www = new WWW(basePath+"/players/login/"+username, null, headers);
		
		float elapsedTime = 0.0f;
		
		while (!www.isDone) {
			elapsedTime += Time.deltaTime;
			
			if (elapsedTime >= 10.0f) break;
			
			yield return null;  
		}
		
		if (!www.isDone || !string.IsNullOrEmpty (www.error)) {
			Debug.LogError (string.Format ("Failed Login call!\n{0}", www.error));
			if(!LanguageManager.Instance.LoadedLanguage.Equals("it"))
				invalidUsername.text = "Invalid username or password!";
			else
				invalidUsername.text = "Credenziali non valide!";
			yield break;
		} else {
			invalidUsername.text = "";
			string response = www.text;

			if(GameController.debug)
				Debug.Log (elapsedTime + " : " + response);    
			
			IDictionary search = (IDictionary)Json.Deserialize (response);
			
			string nickname = (string)search ["nickname"];
			
			chosenEmail = username;
			chosenPassword = password;
			if(GameController.debug)
				Debug.Log ("Retrieved nickname is: " + nickname);
			isAuthenticated = true;
			if(null==scanner)
				scanner = GetComponent<DropCodeScanner>();
			scanner.previous = previous;
			scanner.nextNet = nextStd;
			scanner.nextStd = nextNet;
			scanner.error = message;
			scanner.errorString = messageText;
			//Open a game session since we are online
			yield return instance.StartCoroutine(ogpsRoutine());
			scanner.scanCode();

		}
		
	}


	public void openGameplaySession() {
		instance.StartCoroutine(ogpsRoutine());
	}


	//Open a gameSession for Drop!. The id is stored in the currentSessionID variable.
	IEnumerator ogpsRoutine() {


		WWWForm form = new WWWForm();
		Dictionary<string,string> headers = form.headers;
		
		headers["Authorization"] = "Basic " + System.Convert.ToBase64String(
			System.Text.Encoding.ASCII.GetBytes(chosenEmail+":"+chosenPassword));
		form.AddField ("email", chosenEmail);
		form.AddField ("title", "drop");
		byte[] rawData = form.data;



		
		WWW www = new WWW(basePath+"/gameplay/gamesession/", rawData, headers);
		
		float elapsedTime = 0.0f;
		
		while (!www.isDone) {
			elapsedTime += Time.deltaTime;
			
			if (elapsedTime >= 10.0f) break;
			
			yield return null;  
		}
		
		if (!www.isDone || !string.IsNullOrEmpty (www.error)) {
			Debug.LogError (string.Format ("Failed Gamesession Open call!\n{0}", www.error));
			yield break;
		} else {
			
			string response = www.text;

			if(GameController.debug)
				Debug.Log (elapsedTime + " : " + response);    
			
			IDictionary search = (IDictionary)Json.Deserialize (response);

			if(GameController.debug)
				Debug.Log ("Retrieved gamesession:" + search);

			currentSessionId = (string)search ["_id"];
			
			yield return currentSessionId;

		}
		
	}
	
	public void closeGameplaySession(string sessionID) {
	 	instance.StartCoroutine(cgpsRoutine(sessionID));
	}

	//Closes a gameplaySession with id specified
	IEnumerator cgpsRoutine(string sessionID) {
		WWWForm form = new WWWForm();
		Dictionary<string,string> headers = form.headers;

		sessionID=sessionID.Replace ("ObjectId\"", "");
		sessionID=sessionID.Replace ("\")", "");

		headers["Authorization"] = "Basic " + System.Convert.ToBase64String(
			System.Text.Encoding.ASCII.GetBytes(chosenEmail+":"+chosenPassword));
		string url = basePath+"/gameplay/gamesession/"+sessionID+"/close";
		WWW www = GET (url, headers);
		float elapsedTime = 0.0f;

		while (!www.isDone) {
			elapsedTime += Time.deltaTime;
			
			if (elapsedTime >= 10.0f) break;
			
			yield return null;  
		}
		
		if (!www.isDone || !string.IsNullOrEmpty (www.error) || sessionID.Equals("")) {
			Debug.LogError (string.Format ("Failed Gamesession Close call!\n{0}", www.error));
			yield break;
		} else {
			
			string response = www.text;

			if(GameController.debug)
				Debug.Log (elapsedTime + " : " + response);    
			
			IDictionary search = (IDictionary)Json.Deserialize (response);
			
			sessionID = "";

			if(GameController.debug)
				Debug.Log ("Closed gamesession:" + search);
		}
		
	}

	public static IEnumerator WaitForRequest(WWW www)
	{
		yield return www;
		
		// check for errors
		if (www.error == null)
		{
			if(GameController.debug)
				Debug.Log("WWW Ok!: " + www.text);
		} else {
			Debug.LogError("WWW Error: "+ www.error);
		}    
	}

	public void addPointsAction(int points) {
		instance.StartCoroutine(apaRoutine(points,true));
	}

	public static GameController get() {
				return instance;
		}

	IEnumerator apaRoutine(int points,bool openClose) {

			if (openClose) {
						if (currentSessionId != null) {
								WWWForm form = new WWWForm ();
								Dictionary<string,string> headers = form.headers;
					
								headers ["Authorization"] = "Basic " + System.Convert.ToBase64String (
								System.Text.Encoding.ASCII.GetBytes (chosenEmail + ":" + chosenPassword));
								form.AddField ("gameplayId", currentSessionId);
								form.AddField ("description", "Adding points for quiz submission");
								form.AddField ("points", points);
								byte[] rawData = form.data;
					
					
								WWW www = new WWW (basePath + "/gameplay/action/", rawData, headers);
					
								float elapsedTime = 0.0f;
					
								while (!www.isDone) {
										elapsedTime += Time.deltaTime;
						
										if (elapsedTime >= 10.0f)
												break;
						
										yield return null;  
								}

								IDictionary retrievedAction;
								if (!www.isDone || !string.IsNullOrEmpty (www.error)) {
										Debug.LogError (string.Format ("Failed to submit points!\n{0}", www.error));
										yield break;
								} else {
						
										string response = www.text;
										
										if(GameController.debug)
											Debug.Log (elapsedTime + " : " + response);    
						
										retrievedAction = (IDictionary)Json.Deserialize (response);
										
										if(GameController.debug)
											Debug.Log ("Retrieved action:" + retrievedAction);
						

								}
								yield return retrievedAction;
						}
				
				} else {
						throw new System.ApplicationException("Feature currently not supported");
				}
	}

	public void skip() {
		isSkipped = true;
		DropCodeScanner scanner = GetComponent<DropCodeScanner> ();
		scanner.previous = previous;
		scanner.nextNet = nextStd;
		scanner.nextStd = nextNet;
		scanner.error = message;
		scanner.errorString = messageText;
		scanner.scanCode();
		}



	// Update is called once per frame
	void Update () {
		ConnectionTesterStatus status = Network.TestConnection();
		switch (status.ToString()) {
		case "Error":
		case "Undetermined":inetAvail=false;break;
		case "PublicIPIsConnectable":
		case "PublicIPPortBlocked":
		case "PublicIPNoServerStarted":
		case "LimitedNATPunchthroughPortRestricted":
		case "LimitedNATPunchthroughSymmetric":
		case "NATpunchthroughFullCone":
		case "NATpunchthroughAddressRestrictedCone":inetAvail=true;break;
		default:inetAvail=false;break;
				}
		connection.text = "Connection: " + status.ToString();
		if (inetAvail)
			wifiStatus.SetActive (true);
		else
			wifiStatus.SetActive (false);
	}



	public static WWW GET(string url, Dictionary<string,string> headers)
	{
		
		WWW www = new WWW (url,null, headers);
		StaticCoroutine.DoCoroutine(WaitForRequest (www));
		return www; 
	}



	// Use this for initialization
	public static void addActionGe () {
		if (chosenEmail != null && chosenEmail != "") {
						long currentTs = (long)(DateTime.UtcNow - new DateTime (1970, 1, 1)).TotalMilliseconds;
			string input = "{\"actions\":[[{\"email\":\"" + chosenEmail + "\",\"time\":" + currentTs + ",\"area\":\"Water Saving Insights\",\"name\":\"Drop! correct answer\",\"description\":\"User inputs a correct answer playing the Drop!TheQuestion game\", \"tag\":\" \",\"link\":\" \",\"executor\":\" \"}]]}";
		
						Dictionary<string,string> headers = new Dictionary<string,string> ();
						headers.Add ("Content-Type", "application/json");
		
						byte[] body = Encoding.UTF8.GetBytes (input);
		
						WWW www = new WWW (geBasePath + geActionPath, body, headers);
		
						StaticCoroutine.DoCoroutine (WaitForRequest (www));
				}
	}


	public static string getAdmin() { return adminUser; }

	public static string getAdminPass() { return adminPassword; }
	

}
