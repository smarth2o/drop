﻿using UnityEngine;
using System.Collections;

public class GlobalUtils : MonoBehaviour {

	public CanvasGroup previous,next;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void nextMenu() {
				if ((previous) && (next)) {
						enableCanvas (previous, false);
						enableCanvas (next, true);
				}
		}

	public void quit() 
	{
		GameController instance = GameController.get ();
		instance.quitGame ();
		Application.Quit ();
	}

	public static void enableCanvas(CanvasGroup chosen,bool value) {
		if (value) {
			chosen.interactable = true;
			chosen.alpha = 1;
			chosen.blocksRaycasts = true;
		} else {
			chosen.interactable = false;
			chosen.alpha = 0;
			chosen.blocksRaycasts = false;
		}
	}
}
