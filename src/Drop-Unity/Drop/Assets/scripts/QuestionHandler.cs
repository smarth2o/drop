﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Text;


public class QuestionHandler : MonoBehaviour {

	int reply = 0;
	public CanvasGroup current;
	public CanvasGroup correct;
	public CanvasGroup wrong;
	public Text correctPoints;
	public Text wrongPoints;


	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void chosenAnswer(int number) {
		GameObject go = GameObject.Find ("Question");
		QuestionLoader questionLoader = go.GetComponent <QuestionLoader> ();
		reply = questionLoader.getReply ();
		GlobalUtils.enableCanvas(current,false);
		GameController instance = GameController.get ();
		if (number.Equals (reply)) {
			if (GameController.isAuthenticated) {
				instance.addPointsAction(10);
			}
			GameController.totalPoints+=10;
			correctPoints.text = GameController.totalPoints.ToString();
			GlobalUtils.enableCanvas(correct,true);
			GlobalUtils.enableCanvas(wrong,false);

			GameController.addActionGe();
		}
		else {
			if (GameController.isAuthenticated) {
				instance.addPointsAction(0);
			}
			wrongPoints.text = GameController.totalPoints.ToString();
			GlobalUtils.enableCanvas(correct,false);
			GlobalUtils.enableCanvas(wrong,true);
		}
		questionLoader.newQuestion ();
	}


}
