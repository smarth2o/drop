﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using MiniJSON;

public class QuestionLoader : MonoBehaviour {

	public Text question;
	public Text answer1;
	public Text answer2;
	public Text answer3;
	public Text answer4;
	int reply = 0;


	// Use this for initialization
	void Start () {
		newQuestion ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void newQuestion () {
		//If the network is not available fallback to the questions from the database
		if (!GameController.inetAvail) {
					loadFromInternalDb();
		} else {
					GameController.get ().StartCoroutine(loadQuestionRoutine());
		}
	}

	private void loadFromInternalDb() {
				SqliteDatabase sqlDB = new SqliteDatabase ("questions.db");
				string query = "";
				if (Application.systemLanguage.ToString ().Equals ("Italian"))
						query = "SELECT * FROM DOMANDE";
				else
						query = "SELECT * FROM QUESTIONS";
				DataTable retrieved = sqlDB.ExecuteQuery (query);
				if (retrieved.Rows.Count > 0) {
						int row = Random.Range (1, retrieved.Rows.Count) - 1;
						question.text = retrieved.Rows [row] [retrieved.Columns [0].ToString ()].ToString ();
						answer1.text = retrieved.Rows [row] [retrieved.Columns [1].ToString ()].ToString ();
						answer2.text = retrieved.Rows [row] [retrieved.Columns [2].ToString ()].ToString ();
						answer3.text = retrieved.Rows [row] [retrieved.Columns [3].ToString ()].ToString ();
						answer4.text = retrieved.Rows [row] [retrieved.Columns [4].ToString ()].ToString ();
						reply = (int)retrieved.Rows [row] [retrieved.Columns [5].ToString ()];
				}
		}

	private IEnumerator loadQuestionRoutine() {

				//Recover the question from the DB
				WWWForm form = new WWWForm ();
				Dictionary<string,string> headers = form.headers;
				headers ["Authorization"] = "Basic " + System.Convert.ToBase64String (
				System.Text.Encoding.ASCII.GetBytes (GameController.getAdmin() + ":" + GameController.getAdminPass()));
	
				string languageCode = "en";
				if (Application.systemLanguage.ToString ().Equals ("Italian"))
						languageCode = "it";
				else
						languageCode = "en";
	
	
				WWW www = new WWW (GameController.basePath + "/questions/" + languageCode, null, headers);
				
				float elapsedTime = 0.0f;
	
				while (!www.isDone) {
						elapsedTime += Time.deltaTime;
		
						if (elapsedTime >= 10.0f)
								break;
						yield return null; 
				}
	
	
				if (!www.isDone || !string.IsNullOrEmpty (www.error)) {
						Debug.LogError (string.Format ("Failed to retrieve the question!\n{0}", www.error));
						loadFromInternalDb();
				} else {
						string response = www.text;

						if(GameController.debug)
							Debug.Log (elapsedTime + " : " + response);    
		
						IList questions = (IList)Json.Deserialize (response);
						
						if(questions.Count>=1) {
							int row = Random.Range (0, questions.Count);
							IDictionary retrieved = (IDictionary)questions [row];
							question.text = (string)retrieved ["question"]; 
							answer1.text = (string)((IList)retrieved ["answers"]) [0];
							answer2.text = (string)((IList)retrieved ["answers"]) [1];
							answer3.text = (string)((IList)retrieved ["answers"]) [2];
							answer4.text = (string)((IList)retrieved ["answers"]) [3];
							reply = int.Parse ((string)retrieved ["solution"]);
						}
						else {
							loadFromInternalDb();
						}
				}
		}


	public int getReply() {
		return reply;
	}
}
