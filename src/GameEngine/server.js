// Libraries import
var express = require('express');
var https = require('https');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var mainDomain = require('domain').create();
var fs = require('fs');
var passport = require('passport');

//Controllers import
var authController = require('./controllers/auth');
var playerController = require('./controllers/player');
var gameplayController = require('./controllers/gameplay');
var questionsController = require('./controllers/questions');

//Models import
var User = require('./models/player');
var Game = require('./models/game');

// Create our Express application
var app = express();
var server = null;

var options = {
    key  : fs.readFileSync('./keys/server.key'),
    ca   : fs.readFileSync('./keys/server.csr'),
    cert : fs.readFileSync('./keys/server.crt')
};


//Defines server restart routines
mainDomain.on('error', function (err) {
    console.log("domain caught", err);
    console.log("Trying to restart in 5 seconds...");
    mongoose.connection.close();
    if(server)
        server.close();
    setTimeout(mainFunction, 5000);
});


var mainFunction = mainDomain.bind(function() {


    // Connect to the beerlocker MongoDB
    mongoose.connect('mongodb://localhost:27017/gamePlatform');

    // Use the body-parser package in our application
    app.use(bodyParser.urlencoded({
        extended: true
    }));


    console.log("Drop! Backend Framework v1.0");
    app.use(passport.initialize());

// Create our Express router
    var router = express.Router();

    router.route('/players')
        .post(playerController.postPlayer)
        .get(authController.isAuthenticated, require('permission')(), playerController.getPlayers);

    router.route('/players/login/:email')
        .get(authController.isAuthenticated, require('permission')(), playerController.loginPlayer);

    router.route('/players/:player_id')
        .get(authController.isAuthenticated, require('permission')(['player', 'admin']), playerController.getPlayer);

    router.route('/players/validate/:validate_token')
        .get(playerController.validate);


    //Binding gameplay controller
    router.route('/gameplay/action')
        .post(authController.isAuthenticated, require('permission')(['player', 'admin']), gameplayController.postAction);

    router.route('/gameplay/gamesession')
        .post(authController.isAuthenticated, require('permission')(['player', 'admin']), gameplayController.postGamesession);

    router.route('/gameplay/game')
        .post(authController.isAuthenticated, require('permission')(['admin']), gameplayController.postGame);

    router.route('/gameplay/gamesession/:session_id/close')
        .get(authController.isAuthenticated, require('permission')(['player', 'admin']), gameplayController.closeGamesession);


    //Binding questions controller
    router.route('/questions')
        .post(authController.isAuthenticated, require('permission')(['admin']),questionsController.postQuestion);

    router.route('/questions/:language')
        .get(authController.isAuthenticated, require('permission')(['player', 'admin']),questionsController.getQuestion);



// Register all our routes with /api
    app.use('/api', router);
    createAdmin();
    createGame();
    server = https.createServer(options, app).listen(443);
});


var createAdmin = function() {
    User.findOne({email: 'admin'}, function (err, found) {
        if (found) {
            console.log("Admin account credentials: email:" + found.email + " password: adminsh2o");
        }
        else {
            var user = new User({
                email: 'admin',
                password: 'adminsh2o',
                nickname: 'admin',
                role: 'admin',
                active: true
            });

            user.save(function (err) {
                if (err)
                    console.log("Error in creating admin user! " + err);
                else
                    console.log("Admin account created: email:" + user.email + " password:" + user.password);
            });
        }
    });

}

    var createGame = function() {
        Game.findOne({title: 'drop'}, function (err, found) {
            if (found) {
                console.log("Game instance found: " + found.title);
            }
            else {
                var game = new Game({
                    title: 'drop',
                    genre: 'boardgame',
                    minimumPlayers: 1,
                    maximumPlayers: 1
                });

                game.save(function (err) {
                    if (err)
                        console.log("Error in creating the Drop! game " + err);
                    else
                        console.log("Game instance created: " + game.title);
                });
            }
        });
    }


setTimeout(mainFunction, 100);