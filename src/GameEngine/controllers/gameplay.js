// Load required packages
var User = require('../models/player');
var Action = require('../models/action');
var Gameplay = require('../models/gamesession');
var Game = require('../models/game');

// Create endpoint /api/gameplay/action for POST
exports.postAction = function(req, res) {
  Gameplay.findById({ _id: req.body.gameplayId }, function(err, gamesession) {
    if (err)
      res.send(err);
    else if(!gamesession.endDate!=""){
      var action = new Action({
        gameplayId: req.body.gameplayId,
        description: req.body.description,
        points: req.body.points
      });

      //Update the points for the user that has submitted the action
      User.findById(gamesession.playerId, function(err, retrievedUser) {
        if (err)
          res.send(err);
        else {
            retrievedUser.points += action.points;
            retrievedUser.save(function(err) {
              if (err)
                res.send(err);
            });
        }
      });

      action.save(function(err) {
        if (err)
          res.send(err);

        res.json(action);
      });
    }
    else {
      res.send({"error":"The session with id "+gamesession._id+"has been closed already"});
    }
  });
};

// Create endpoint /api/gameplay/gamesession for POST
exports.postGamesession = function(req, res) {
  User.findOne({ email: req.body.email }, function(err, retrievedUser) {
    if (err)
      res.send(err);
    else {
      Game.findOne({ title: req.body.title }, function(err, retrievedGame) {
        if (err||retrievedGame==null||retrievedUser==null)
          res.json({"error":"Cannot retrieve game or user or malformed parameters"});
        else {
          var date = new Date();
          var gamesession = new Gameplay({
            playerId: retrievedUser._id,
            gameId: retrievedGame._id,
            startDate: date.getTime()
          });

          gamesession.save(function(err) {
            if (err)
              res.send(err);

            res.json(gamesession);
          });
        }
      });
    }
  });
};

// Create endpoint /api/gameplay/gamesession/:session_id/close for GET
exports.closeGamesession = function(req, res) {
  var retrievedID = req.params.session_id;
  if (retrievedID.match(/^[0-9a-fA-F]{24}$/)) {
    Gameplay.findById(retrievedID, function(err, gamesession) {
      if (err)
        res.send(err);
      else {
        var date = new Date();
        gamesession.endDate = date.getTime();

        gamesession.save(function(err) {
          if (err)
            res.send(err);
          res.json(gamesession);
        });
      }
    });
  }
  else {
    res.json({"error":"The provided id "+req.param.session_id+" is not a valid Objectid"});
  }
};

// Create endpoint /api/gameplay/gamesession for POST
exports.postGame = function(req, res) {
      Game.findOne({ title: req.body.title }, function(err, retrievedGame) {
        if (err)
          res.send(err);
        else if(retrievedGame) {
          res.json({"error" : "A game with the same title exists already"})
        }
        else {
          var newGame = new Game({
            title: req.body.title
          });

          newGame.save(function(err) {
            if (err)
              res.send(err);

            res.json(newGame);
          });
        }
      });
    };
