// Load required packages
var User = require('../models/player');
var Action = require('../models/action');
var Question = require('../models/question');
var Game = require('../models/game');

// Create endpoint /api/questions/ for POST
exports.postQuestion = function(req, res) {
    Game.findOne({ title: req.body.title }, function(err, retrievedGame) {
        if (err||retrievedGame==null)
            res.json({"error":"Cannot retrieve game or user or malformed parameters "+err});
        else {
            var question = new Question({
                gameId: retrievedGame._id,
                question: req.body.question,
                answers: req.body.answers,
                solution: req.body.solution,
                difficulty: req.body.difficulty,
                region: req.body.region,
                language: req.body.language
            });

            question.save(function(err) {
                if (err)
                    res.send(err);

                res.json(question);
            });
        }
    });
};

// Create endpoint /api/questions/:language for GET
exports.getQuestion = function(req, res) {
    Question.find({ language: req.params.language }, function(err, retrievedQuestions) {
        if (err)
            res.send(err);
        else {
            res.send(retrievedQuestions);
        }
    });
};

