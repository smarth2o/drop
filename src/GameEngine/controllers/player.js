// Load required packages
var User = require('../models/player');
var async = require('async');
var nodemailer = require('nodemailer');
var sgTransport = require('nodemailer-sendgrid-transport');
var crypto = require('crypto');


// Create endpoint /api/players for POST
exports.postPlayer = function(req, res, next) {
  async.waterfall([
    function(done) {
      crypto.randomBytes(20, function(err, buf) {
        var token = buf.toString('hex');
        done(err, token);
      });
    },
    function(token, done) {
      User.findOne({ email: req.body.email }, function(err, found) {
        if (found) {
          res.json('The account is already registered');
        }
        else {
          var user = new User({
            email: req.body.email,
            password: req.body.password,
            nickname: req.body.nickname,
            role: req.body.role,
            name: req.body.surname,
            surname: req.body.surname,
            familyId: req.body.familyId,
            smartMeterId: req.body.smartMeterId,
            points: "0",
            type: "beginner",
            active: false,
            validateAccountToken: token,
            validateAccountExpires: Date.now() + 10800000 // 3 hours
          });

          user.save(function(err) {
            done(err, token, user);
          });
        }
      });
    },
    function(token, user, done) {

      var options = {
        auth: {
          api_user: 'LGMoonsubmarine',
          api_key: 'sendgridpassworddrop15'
        }
      };

      var client = nodemailer.createTransport(sgTransport(options));

      var mailOptions = {
        to: user.email,
        from: 'validateaccount@drop.com',
        subject: 'Newly registered Drop Account',
        text: 'You are receiving this because you (or someone else) have registered an account to access the Drop!Game.\n\n' +
          'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
          'https://' + req.headers.host + '/api/players/validate/' + token + '\n\n' +
          'If you did not request this, please ignore this email and your registration will expire in 3 hours.\n'
      };

      client.sendMail(mailOptions, function(err, info){
          if (err ){
            res.json(err);
          }
          else {
            res.json('Account with email:'+user.email+' has been registered. Validation required.');
          }
      });
    }
  ], function(err) {
    if (err) return next(err);
  });
};

// Create endpoint /api/players for GET
exports.getPlayers = function(req, res) {
  User.find(function(err, users) {
    if (err)
      res.send(err);

    res.json(users);
  });
};

// Create endpoint /api/players/validate/:validate_token for GET
exports.validate = function(req, res) {
  User.findOne({validateAccountToken: req.params.validate_token,  validateAccountExpires: { $gt: Date.now() } }, function(err, user) {
    if (err)
      res.send(err);
    if(null!=user) {
      user.active = true;
      user.validateAccountToken = "";
      user.validateAccountExpires = "";
      user.save(function(err) {
        if (err)
          res.send(err);

        res.json('User with email: '+user.email+' has been activated.');
      });
    }
    else {
      res.json('Invalid token.');
    }
  });
};

// Create endpoint /api/players/:player_id for GET
exports.getPlayer = function(req, res) {
  User.findById({_id: req.params.player_id }, function(err, player) {
    if (err)
      res.send(err);
    res.json(player);
  });
};

// Create endpoint /api/players/:email for GET
exports.loginPlayer = function(req, res) {
    User.findOne({ email: req.params.email }, function(err, found) {
    if (err)
      res.send(err);
    res.json(found);
  });
};
