// Load required packages
var mongoose = require('mongoose');

// Define our user schema
var ActionSchema = new mongoose.Schema({
  gameplayId: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  points: {
    type: Number,
    required: true
  }
});

// Export the Mongoose model
module.exports = mongoose.model('Action', ActionSchema);
