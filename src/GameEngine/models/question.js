// Load required packages
var mongoose = require('mongoose');

// Define our user schema
var QuestionSchema = new mongoose.Schema({
  gameId: {
    type: String,
    required: true
  },
  question: {
    type: String,
    required: true
  },
  answers: [String],
  solution: {
    type: String,
    required: true
  },
  difficulty: {
    type: String,
    required: true
  },
  region: {
    type: String,
    required: true
  },
  language: {
    type: String,
    required: true
  }
});

// Export the Mongoose model
module.exports = mongoose.model('Question', QuestionSchema);
