// Load required packages
var mongoose = require('mongoose');

// Define our user schema
var GameSchema = new mongoose.Schema({
  title: {
    type: String,
    unique: true,
    required: true
  },
  genre: {
    type: String,
  },
  minimumPlayers: {
    type: String,
  },
  maximumPlayers: {
    type: String
  }
});

// Export the Mongoose model
module.exports = mongoose.model('Game', GameSchema);
