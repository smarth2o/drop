// Load required packages
var mongoose = require('mongoose');

// Define our user schema
var GameplaySchema = new mongoose.Schema({
  playerId: {
    type: String,
    required: true
  },
  gameId: {
    type: String,
    required: true
  },
  startDate: {
    type: String
  },
  endDate: {
    type: String
  }
});

// Export the Mongoose model
module.exports = mongoose.model('Gameplay', GameplaySchema);
